﻿using System;
using System.Collections.Generic;

namespace TP2 {
    class Program {
        static void Main (string[] args) {
            Program.Ejercicio1 ();
        }

        public static void Ejercicio1 () {
            Console.WriteLine ("Ejercicio 1");
            Estudiante est = new Estudiante ("Franco", 38911869, 38648);
            Profesor prof = new Profesor ("Romina", 12345678, "Titulo");
            Curso cursonet = new Curso ("Dotnet", 8, prof);
            est.Inscribir (cursonet);
            Console.WriteLine ($"Los inscriptos a {cursonet.Nombre} son: ");
            foreach (string nombre in cursonet.Inscriptos ()) {
                Console.WriteLine ($"·{nombre}");
            }

            Console.ReadLine ();
        }

        public static void Ejercicio2 () {
            Console.WriteLine ("");
            try {
                Programador progr = new Programador ("Franco Ance", "123", 18, false, 20000, 100, "Python");
                progr.MostrarDatos ();
                Console.WriteLine ("Clasificación: " + progr.Clasificacion ());
                Console.WriteLine ("Se aumenta el salario en un 20%");
                progr.AumentarSalarioPorc (20);
                progr.MostrarDatos ();
            } catch (ArgumentException ex) {
                Console.WriteLine (ex.Message);
            }
        }
    }

    class Inscripcion {
        public DateTime Fecha { get; set; }
        public Estudiante alumno { get; set; }
        public Curso en { get; set; }
        public Inscripcion (Estudiante alumno, Curso en) {
            this.alumno = alumno;
            this.en = en;
            this.Fecha = DateTime.Now;
            this.alumno.Registro.Add (this);
            this.en.de.Add (this);
        }
    }

    class Curso {
        public string Nombre { get; set; }
        public int Duracion { get; set; }
        public List<Inscripcion> de { get; set; }
        public Profesor Instructor { get; set; }

        public Curso (string Nombre, int Duracion, Profesor Instructor) {
            this.Nombre = Nombre;
            this.Duracion = Duracion;
            this.Instructor = Instructor;
            this.Instructor.Tema.Add (this);
            de = new List<Inscripcion> (capacity: 50);
        }

        public List<string> Inscriptos () {
            List<string> inscrString = new List<string> (capacity: 50);
            foreach (Inscripcion insc in this.de) {
                inscrString.Add (insc.alumno.Nombre);
            }

            return inscrString;
        }

        public int CantidadInscriptos () {
            return this.de.Count;
        }
    }
    class Persona {
        public string Nombre { get; set; }
        public long DNI { get; set; }

        public Persona (string nombre, long dni) {
            this.Nombre = nombre;
            this.DNI = dni;
        }
    }

    class Estudiante : Persona {

        public long Legajo { get; set; }
        public int CantCursos { get; set; }
        public List<Inscripcion> Registro;

        public Estudiante (string nombre, long dni, long legajo) : base (nombre, dni) {
            this.Registro = new List<Inscripcion> ();
            this.Legajo = legajo;
            this.CantCursos = 0;
        }

        public void Inscribir (Curso curso) {
            Inscripcion insc = new Inscripcion (alumno: this, en: curso);
            this.CantCursos++;
            Console.WriteLine ($"{this.Nombre} inscripto en {curso.Nombre}");
        }
    }

    class Profesor : Persona {
        public string Titulo { get; set; }
        public List<Curso> Tema { get; set; }

        public Profesor (string nombre, long dni, string titulo) : base (nombre, dni) {
            this.Titulo = titulo;
            this.Tema = new List<Curso> (capacity: 3);
        }
    }

    class Empleado {
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public int Edad { get; set; }
        public bool Casado { get; set; }
        public double Salario { get; set; }

        public Empleado () {

        }

        public Empleado (string Nombre, string Cedula, int Edad, bool Casado, double Salario) {
            if (Edad < 18 | Edad > 45) {
                throw new ArgumentException ("La edad debe ser entre 18 y 45 años");
            }
            this.Nombre = Nombre;
            this.Cedula = Cedula;
            this.Edad = Edad;
            this.Casado = Casado;
            this.Salario = Salario;
        }

        public string Clasificacion () {
            if (this.Edad <= 21) {
                return "Principiante";
            } else {
                if (this.Edad < 22 & this.Edad <= 35) {
                    return "Intermedio";
                } else {
                    return "Senior";
                }
            }
        }

        public void MostrarDatos () {
            Console.WriteLine ($"Nombre: {this.Nombre} \n" +
                $"Cedula: {this.Cedula} \n" +
                $"Edad: {this.Edad} \n" +
                $"Casado: {this.Casado} \n" +
                $"Salario: {this.Salario} \n");
        }

        public void AumentarSalarioPorc (double porcentaje) {
            this.Salario += this.Salario * (porcentaje / 100);
        }
    }

    class Programador : Empleado {
        public int LineasDeCodigoPorHora { get; set; }
        public string LenguajeDominante { get; set; }

        public Programador () { }

        public Programador (string Nombre, string Cedula, int Edad,
                bool Casado, double Salario, int LineasDeCodigoPorHora, string LenguajeDominante):
            base (Nombre, Cedula, Edad, Casado, Salario) {
                this.LineasDeCodigoPorHora = LineasDeCodigoPorHora;
                this.LenguajeDominante = LenguajeDominante;
            }
    }
}